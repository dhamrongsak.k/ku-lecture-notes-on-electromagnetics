
[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |  

|[ก่อนหน้า](ch1-05TH.md)| [ต่อไป](ch1-05-2TH.md) |
| ---------- | ---------- |    

## 1.5.1 [การคูณแบบดอท (dot product)](ch1-05-1TH.md)  
การคูณแบบดอทเป็นหนึ่งในหัวข้อของวิชาพิชคณิตเชิงเส้น (linear algebra) คือการหาความสัมพันธ์ของจำนวนเวกเตอร์สองจำนวน โดยไม่ได้จำกัดมิติของเวกเตอร์  
```math
\mathbf X = [x_1, x_2, x_3 ... x_n] \newline
\mathbf Y = [y_1, y_2, y_3 ... y_n] \newline
\mathbf X \cdot \mathbf Y = \sum_{i=1}^{i=n} {x_i y_i} \newline
\mathbf X \cdot \mathbf Y = \mathbf X  \mathbf Y^T
``` 
ความสัมพันธ์ของจำนวนเวกเตอร์สองจำนวนกำหนดโดยค่ามุมระหว่างเวกเตอร์ทั้งสองเรียกชื่อว่า $`\theta`$ 
```math
\mathbf X \cdot \mathbf Y =|\mathbf X|  |\mathbf Y| \cos \theta \newline 
|\mathbf X| = \sqrt{x_1^2+x_2^2+ ... +x_{n-1}^2+x_n^2} \newline
|\mathbf Y| = \sqrt{y_1^2+y_2^2+ ... +y_{n-1}^2+y_n^2} \newline
\cos \theta = \frac{\mathbf X  \mathbf Y^T}{|\mathbf X|  |\mathbf Y|} \newline
``` 


ในระบบหลายมิติ หรือ $`n`$ มิติจะจินตนาการลำบากว่าเวกเตอร์ทั้งสองทำมุมกันอย่างไร แต่ในระบบ 3 มิติโดยเฉพาะในพิกัดฉากเราสามารถวาดรูปได้ดังรูป (ดัดแปลงจาก [Hayt 2011](#Hayt))  
#### ตัวอย่าง  


<img src="./asset/dot1.svg" width="300" >  

จากรูป $`\mathbf r_p =  \mathbf a_x+ 2 \mathbf a_y+ 3 \mathbf a_z`$ และ $`\mathbf r_q = 2 \mathbf a_x- \mathbf a_y+  \mathbf a_z`$ และมุม $`\theta`$ ที่ $`\mathbf r_Q`$ ทำกับ $`\mathbf r_Q`$ ซึ่งเราสามารถหา $`\mathbf r_P \cdot \mathbf r_Q = x_P x_Q +y_P y_Q+z_P z_Q`$ และ $`\mathbf r_P \cdot \mathbf r_Q =|\mathbf r_Q|  |\mathbf r_Q| \cos \theta`$   

```math
\mathbf r_P \cdot \mathbf r_Q = x_P x_Q +y_P y_Q+z_P z_Q \newline
\mathbf r_P \cdot \mathbf r_Q = 1 \cdot 2 +2 \cdot -1 +3 \cdot 1 \newline
\mathbf r_P \cdot \mathbf r_Q = 2 -2+3 = 3\newline
```
```math
|\mathbf r_P| = \sqrt{x_P^2+y_P^2+z_P^2} = \sqrt{1^2+2^2+3^2} = \sqrt{1+4+9} = \sqrt{14} \newline
|\mathbf r_Q| = \sqrt{x_Q^2+y_Q^2+z_Q^2} = \sqrt{2^2+(-1)^2+1^2} = \sqrt{4+1+1} = \sqrt{6} \newline
\mathbf r_P \cdot \mathbf r_Q =|\mathbf r_Q|  |\mathbf r_Q| \cos \theta \newline
```
จาก $`\mathbf r_P \cdot \mathbf r_Q = 3`$ ,  $`|\mathbf r_P| = \sqrt{14}`$ และ $`|\mathbf r_Q| = \sqrt{x_Q^2+y_Q^2+z_Q^2} = \sqrt{6}`$ เราจะได้ว่า  
```math
\mathbf r_P \cdot \mathbf r_Q =|\mathbf r_Q|  |\mathbf r_Q| \cos \theta \newline
\theta =  \cos^{-1} \left(\frac{\mathbf r_P \cdot \mathbf r_Q}{|\mathbf r_P|  |\mathbf r_Q|}\right)  \newline
\theta =  \cos^{-1} \left(\frac{3}{\sqrt{14}  \sqrt{6}}\right)  \newline
```  
เราสามารถหาค่า $`\cos^{-1} \left(\frac{3}{\sqrt{14}  \sqrt{6}}\right)`$ ได้จากเว็บไซต์ [octave-online](https://octave-online.net/) โดยพิมพ์คำสั่งตามด้านล่างซึ่งจะได้คำตอบ $`\theta = 1.2372 \text {rad}`$ แปลงเป็นองศาโดยคูณ $`\frac{180}{\pi}`$ จะได้ $`\theta = 70.893^o`$  
```
octave:1> acos(3/(sqrt(14)*sqrt(6)))
ans = 1.2373
octave:2> acos(3/(sqrt(14)*sqrt(6)))*180/pi
ans = 70.893
```  

เราสามารถหาส่วนของเวกเตอร์ที่ชี้ไปในทิศทางใดทิศทางหนึ่งได้โดย นำเวกเตอร์นั้นคูณแบบดอทกับเวกเตอร์หนึ่งหน่วยในทิศทางที่ต้องการหาเช่น รูปด้านล่างแสดงการหาส่วนของเวกเตอร์ $`\mathbf C `$ ในทิศทาง $`\mathbf a`$ โดยรูป (a) แสดงให้เห็นว่า $`\mathbf C \cdot \mathbf a`$ เป็นขนาดของ $`\mathbf C`$ ในทิศทาง $`\mathbf a`$ ซึ่งเป็นปริมาณสเกลาร์ เหมือนกัยเราฉายไฟฉายบน เวกเตอร์ $`\mathbf C`$ แล้วเงาของ เวกเตอร์ $`\mathbf C`$ ตกลงมาบน $`\mathbf a`$ ถ้าเราต้องการส่วนประกอบเวกเตอร์ของเวกเตอร์ $`\mathbf C `$ ในทิศทาง $`\mathbf a`$ ซึ่งมีทั้งขนาดและทิศทาง เราต้องทำตามรูป (b) โดยคูณขนาด ขนาดของ $`\mathbf C`$ ในทิศทาง $`\mathbf a`$ ด้วยตัวบอกทิศทางคือเวกเตอร์หนึ่งหน่วย $`\mathbf a`$ 

<img src="./asset/dot2.svg" width="600" >  

การคูณแบบดอทนำมาประยุกต์ใช้ในการหางาน (work: W) ซึ่งมีค่าเท่ากับแรง $`\mathbf F`$ คูณกับระยะทาง $`\mathbf L`$ ในทิศทางเดียวกัน หรือ $`W = \mathbf F \cdot \mathbf L`$ ถ้าแรงมีค่าไม่คงที่เปลี่ยนแปลงตามตำแหน่งหรือค่า $`x,y,z`$ เราจะต้องคำนวนงานในทุกๆตำแหน่งโดยเราอยากแบ่งระยะทาง $`\mathbf L`$ เป็นระยะทางสั้นๆ $`N`$ ชิ้นโดย $`\Delta \mathbf L_i`$ เป็นระยะทางสั้นๆชิ้นที่ $`i`$ เราจะหางานโดยประมาณได้จาก  
```math
W \approx \sum_{i=1}^{i=N} \mathbf F \cdot \Delta \mathbf L_i   
```
โดย $`\Delta \mathbf L_i`$ สามารถเขียนอยู่ในรูปของส่วนประกอบเวกเตอร์ของเวกเตอร์ในพิกัดฉากคือ $`\Delta \mathbf L_i=  \Delta x_i \mathbf a_x + \Delta y_i \mathbf a_y+\Delta z_i \mathbf a_z`$  ถ้าเราต้องการให้ได้ค่าจริงของงานเราจะต้องแบ่งระยะทางให้เล็กที่สุดเท่าที่จะแบ่งได้ซึ่งจำนวนชิ้นของระยะทางสั้นๆจะเป็นอนันต์ $`N = \infty`$ โดยจะมีสมการเป็น  
```math
W = \sum_{i=1}^{i=\infty} \mathbf F \cdot (\Delta x_i \mathbf a_x + \Delta y_i \mathbf a_y+\Delta z_i \mathbf a_z)   
```
ซึ่งเราไม่สามารถเอาค่ามารวมกันเป็นอนันต์(infinite) ครั้ง แต่เราจะเปลี่ยนเครื่องหมาย summation เป็นเครื่องหมาย integrate แทนและเปลี่ยน $`{\Delta x_i}`$ เป็น $`{dx}`$ และ $`{\Delta y_i}`$ เป็น $`{dy}`$ และ $`{\Delta z_i}`$ เป็น $`{dz}`$ จะได้สมการหางานจากแรงไม่คงที่ดังนี้
```math
W = \int_{x_1,y_1,z_1}^{x_2,y_2,z_2} \mathbf F \cdot (dx \mathbf a_x + dy \mathbf a_y+dz \mathbf a_z)   
```
จุด $`(x_1,y_1,z_1)`$ เป็นจุดเริ่มต้นในการทำงาน และจุด $`(x_2,y_2,z_2)`$ เป็นจุดสุดท้ายในการทำงาน เส้นทางระหว่างจุดเริ่มต้นกับจุดสุดท้าย จะเป็นสมการความสัมพันธ์ระหว่าง $`x,y,z`$ หรือ $`x = f(y,z)`$ ซึ่งเราจะหาความสัมพันธ์ระหว่าง $`dx,dy,dz`$ ได้ถ้าเส้นทางระหว่างจุดเริ่มต้นกับจุดสุดท้ายเปลี่ยนไปงานที่ได้อย่างเปลี่ยนไปด้วย  
[ตัวอย่างการหางานจากการออกแรงดันก้อนหินต้านแรงเสียดทาน](ch1-05-1-1TH.md)  

รายละเอียดเพิ่มเติมสามารถดูได้ที่  
- [ocw.mit.edu](https://ocw.mit.edu/courses/mathematics/18-02sc-multivariable-calculus-fall-2010/1.-vectors-and-matrices/part-a-vectors-determinants-and-planes/session-2-dot-products)  
- <a name="Hayt"></a> Hayt, W. H., & Buck, J. A. (2011). Engineering Electromagnetics (8th ed.). McGraw-Hill Professional.

|[ก่อนหน้า](ch1-05TH.md)| [ต่อไป](ch1-05-2TH.md) |
| ---------- | ---------- |


[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


