|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |   

|[ก่อนหน้า](ch1-03TH.md)| [ต่อไป](ch1-04TH.md) |
| ---------- | ---------- |    

### 1.3.4 [ตัวอย่างการน้ำหนักจากความหนาแน่น](ch1-03-04TH.md)  
<a href="example_straight_line_density_integration.ipynb" target="_blank">Click เปิดหน้าต่างใหม่</a>





|[ก่อนหน้า](ch1-03TH.md)| [ต่อไป](ch1-04TH.md) |
| ---------- | ---------- | 
